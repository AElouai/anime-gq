const express = require("express");
const cors = require("cors");
const graphQLHTTP = require("express-graphql");
const { GraphQLSchema } = require("graphql");

const Query = require("./types/Query");
const AnimeModal = require("./model/Anime");

const schema = new GraphQLSchema({
  query: Query
});

let app = express();

app.use(cors());

app.use(
  "/backend",
  graphQLHTTP({
    schema,
    graphiql: true
  })
);

let port = process.env.PORT || 4500;

app.listen({ port }, () => {
  // const an = new AnimeModal({
  //   type: 4,
  //   producers: "ali",
  //   title: "ali in IDTGV",
  //   image:
  //     "https://www.google.fr/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwjf46Of5o3fAhVGXRoKHaqYCTEQjRx6BAgBEAU&url=http%3A%2F%2Fcollider.com%2Fnew-once-upon-a-deadpool-poster%2F&psig=AOvVaw0sLlSWxdkHvx5oRjg1chgP&ust=1544274845954254"
  // });
  // an.save(function(err, data) {
  //   console.log(`🚀 Save Anime :`, err, data);
  // });
  AnimeModal.getAnime("Nekopara").exec(function(err, data) {
    console.log(`🚀 AnimeModal :`, err, data);
  });
  console.log(`🚀 Server ready at port http://localhost:${4500}/backend`);
});
