const mongoose = require("mongoose");

mongoose.Promise = global.Promise;

let url = process.env.MONGODB_URI || "mongodb://localhost/anime";

const options = {
  useNewUrlParser: true
};

mongoose.connect(
  url,
  options,
  function() {
    console.log(`🚀 🚀  mongodb connected ${url}`);
  }
);

module.exports = mongoose;
