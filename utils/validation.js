var validator = require("email-validator");

module.exports = function(email) {
  email: validator.validate(email);
};
