const db = require("../db");
const Schema = db.Schema;

var animeSchema = new Schema({
  type: { type: Number },
  producers: { type: String },
  title: { type: String },
  image: { type: String }
});

var AnimeModal = db.model("anime", animeSchema);

module.exports = {
  getAnime: title => {
    return AnimeModal.findOne({ title: { $regex: title } });
  },
  getAnimes: title => {
    return AnimeModal.find({ title: { $regex: title } });
  }
};
