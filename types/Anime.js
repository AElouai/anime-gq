const {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
  GraphQLID
} = require("graphql");

const AnimeModel = require("../model/Anime");

const fields = {
  type: {
    type: GraphQLInt,
    description: "Type of the anime",
    // resolve: anime => anime.type
    resolve: anime => {
      console.log("anime", anime.type);
      return anime.type;
    }
  },
  producers: {
    type: GraphQLString,
    description: "producers of the anime",
    resolve: anime => anime.producers
  },
  title: {
    type: GraphQLString,
    description: "title of the anime",
    resolve: anime => anime.title
  },
  image: {
    type: GraphQLString,
    description: "image of the anime",
    resolve: anime => anime.image
  }
};

const Anime = new GraphQLObjectType({
  name: "Anime",
  description: "Anime type definition",
  fields: fields
});

const GetAnimeType = {
  type: Anime,
  args: {
    title: {
      type: GraphQLString
    }
  },
  resolve: (_, args) => {
    console.log("resolve", args);
    return AnimeModel.getAnime(args.title);
  }
};
const GetAnimesType = {
  type: GraphQLList(Anime),
  args: {
    title: {
      type: GraphQLString
    }
  },
  resolve: (_, args) => {
    console.log("resolve", args);
    return AnimeModel.getAnimes(args.title);
  }
};

module.exports = {
  GetAnimeType,
  GetAnimesType
};
