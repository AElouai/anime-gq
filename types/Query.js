const { GraphQLObjectType } = require("graphql");

const Anime = require("./Anime");

const Query = new GraphQLObjectType({
  name: "Query",
  description: "Query interface for our App",
  fields: {
    getAnime: Anime.GetAnimeType,
    getAnimes: Anime.GetAnimesType
  }
});

module.exports = Query;
